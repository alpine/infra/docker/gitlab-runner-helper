FROM registry.alpinelinux.org/img/alpine:edge

ENV SSL_CERT_FILE=/etc/ssl/cert.pem

RUN apk add --no-cache git gitlab-runner-helper \
    && git config --global init.defaultBranch master

COPY scripts/gitlab-runner-build /usr/bin
