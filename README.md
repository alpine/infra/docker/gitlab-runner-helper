# gitlab-runner-helper

[![pipeline status](https://gitlab.alpinelinux.org/alpine/infra/docker/gitlab-runner-helper/badges/master/pipeline.svg)](https://gitlab.alpinelinux.org/alpine/infra/docker/gitlab-runner-helper/commits/master)

This builds the gitlab-runner-helper docker image required for running
gitlab-runner with the docker executor.

The default image available on Docker Hub only supports x86_64 and ARM, while we
require more arches.

This will use the gitlab-runner-helper binary from Aports.
